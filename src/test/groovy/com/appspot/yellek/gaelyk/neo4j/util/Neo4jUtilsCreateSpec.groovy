package com.appspot.yellek.gaelyk.neo4j.util

import groovyx.gaelyk.logging.GroovyLogger
import groovyx.gaelyk.spock.*
import groovy.json.JsonOutput


class Neo4jUtilsCreateSpec extends GaelykUnitSpec {
	
	def log = new GroovyLogger("com.appspot.yellek.gaelyk.neo4j.util.Neo4jUtilsCreateSpec")
	
	def userid = "userid"
	def password = "password"
	def baseUri = "http://dummy.com/db"
	

	def setup() {

	}
	
	def "authorization header value is correct"() {
		setup: "the utility class is created"
		def neo4jUtils = new Neo4jUtils(userid, password, baseUri)
		
		when: "the authorization header is queried"
		def header = neo4jUtils.authorizationHeaderValue
		
		then: "the header value is correct"
		header.startsWith("Basic ")
		header.endsWith("$userid:$password".bytes.encodeBase64().toString())
	}
	
	def "create message is correctly constructed"() {
		setup: "the utility class is created"
		def neo4jUtils = new Neo4jUtils(userid, password, baseUri)
		def createJson = '''{
    "statements": [
        {
            "statement": "CREATE (n:LABEL1:LABEL2 {props}) RETURN id(n)",
            "parameters": {
                "props": {
                    "foo": "bar",
                    "bar": "baz",
                    "fizz": 67
                }
            }
        }
    ]
}'''
		
		when: "a create JSON is created"
		def properties = ['foo' : 'bar', 'bar':'baz', 'fizz':67]
		def labels = ["LABEL1", "LABEL2"]
		def json = JsonOutput.prettyPrint(neo4jUtils.createNodeJson(properties, labels))
		
		then: "the create json is correct"
		json == createJson

	}
	
	def "stack overflow error test"() {
		setup: "the utility class is created"
		def neo4jUtils = new Neo4jUtils(userid, password, baseUri)
		def createJson = '''{
    "statements": [
        {
            "statement": "CREATE (n:Principle {props}) RETURN id(n)",
            "parameters": {
                "props": {
                    "identifier": "id-261"
                }
            }
        }
    ]
}'''
		
		when: "a create JSON is created"
		def properties = ['identifier' : 'id-261']
		def labels = ["Principle"]
		def json = JsonOutput.prettyPrint(neo4jUtils.createNodeJson(properties, labels))
		
		then: "the create json is correct"
		json == createJson

	}

}
