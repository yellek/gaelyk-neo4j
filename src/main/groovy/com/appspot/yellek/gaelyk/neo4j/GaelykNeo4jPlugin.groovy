/**
 * Plugin descriptor for the neo4j plugin
 */
package com.appspot.yellek.gaelyk.neo4j

import org.neo4j.ogm.session.SessionFactory
 
//TODO: allow a different domain to be specified in the ogm.properties file
SessionFactory sessionFactory = new SessionFactory("default.domain")

// add new variables in the binding
binding {
	neo4j = sessionFactory
}
 
// add new routes with the usual routing system format
routes {
}
 
before {
}
 
after {
}
 
// any other initialization code you'd need
// ...