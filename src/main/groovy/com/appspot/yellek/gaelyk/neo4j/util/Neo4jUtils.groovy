package com.appspot.yellek.gaelyk.neo4j.util

import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovyx.gaelyk.logging.GroovyLogger
import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.Method.POST
import static groovyx.net.http.ContentType.JSON

/**
 * Utility class for working with Neo4j databases
 * @author Peter Kelley
 *
 */
class Neo4jUtils {

	String userid
	String password
	String connectionHost
	String baseUri
	def httpBuilder
	static int TEN_SECONDS  = 10*1000
	static int THIRTY_SECONDS  = 30*1000
	
	def log = new GroovyLogger("com.appspot.yellek.gaelyk.neo4j.util.Neo4JUtils")

	Neo4jUtils(userid, password, connectionHost, baseUri) {
		this.userid = userid
		this.password = password
		this.connectionHost = connectionHost
		this.baseUri = baseUri
		
		httpBuilder = new HTTPBuilder( connectionHost )
		httpBuilder.auth.basic userid, password
		httpBuilder.getClient().getParams().setParameter("http.connection.timeout", new Integer(THIRTY_SECONDS))
		if (System.getProperty('http.proxyHost')) {
			httpBuilder.setProxy(System.getProperty('http.proxyHost'), System.getProperty('http.proxyPort').toInteger(), 'http')
			log.info "http proxy set to ${System.getProperty('http.proxyHost')}:${System.getProperty('http.proxyPort')}"
		}

	}
	
	def getAuthorizationHeaderValue() {
		def encoded = "$userid:$password".bytes.encodeBase64().toString()
		return "Basic $encoded"
	}
	
	/**
	 * Create a node in the database
	 * @param properties The properties for the node
	 * @param labels the labels to be applied to the node
	 * @return the ID of the node in the database (not currently implemented)
	 */
	int createNode(properties, labels) {
		def json = createNodeJson(properties, labels)
		log.info "Sending create node call"
		def response = sendDatabaseRequest(json)
		return 0 //TODO: return the current node ID instead of 0
	}
	
	/**
	 * Create a node in the database
	 * @param relType the type of the relationship
	 * @param idProperty the name of the identifier property
	 * @param sourceId the value of the id property for the source node
	 * @param targetId the value of the id property for the target node
	 * @param properties The properties for the relationship
	 * @return the ID of the relationship in the database (not currently implemented)
	 */
	int createRelationship(relType, idProperty, sourceId, targetId, properties) {
		def json = createRelationshipJson(relType, idProperty, sourceId, targetId, properties)
		log.info "Sending create relationship call"
		def response = sendDatabaseRequest(json)
		return 0 //TODO: return the current node ID instead of 0
	}
	
	/**
	 * Create the JSON necessary to create a node in the database
	 * @param properties The properties of the node as a map
	 * @param labels The labels to be applied to the node (if any) as a list.
	 * @return The JSON as a String
	 */
	String createNodeJson(properties, labels) {
		log.info "Starting JSON Generation"
		log.info "properties: $properties"
		log.info "labels: $labels"
		def labelString = ''
		labels.each {
			labelString += ":$it"
		}
		def cypherStatements = [['statement' : "CREATE (n$labelString {props}) RETURN id(n)", 'parameters' : ['props' : properties]]]
		log.info "Cypher statements: $cypherStatements"
		def json = new JsonBuilder()
		
		json {statements cypherStatements}
		log.info json.toString()
		return json.toString()
	}
	
	/**
	 * Create the JSON necessary to create a node in the database
	 * @param relType the type of the relationship
	 * @param idProperty the name of the identifier property
	 * @param sourceId the value of the id property for the source node
	 * @param targetId the value of the id property for the target node
	 * @param properties The properties for the relationship
	 * @return The JSON as a String
	 */
	String createRelationshipJson(relType, idProperty, sourceId, targetId, properties) {
		log.info "Starting create relationship JSON generation"
		log.info "relType: $relType"
		log.info "idProperty: $idProperty"
		log.info "sourceId: $sourceId"
		log.info "targetId: $targetId"
		log.info "properties: $properties"

		def statement = "MATCH (a),(b) WHERE a.$idProperty = '$sourceId' AND b.$idProperty = '$targetId' CREATE (a)-[r:$relType {props}]->(b) RETURN r"
		def cypherStatements = [['statement' : statement, 'parameters' : ['props' : properties]]]
		log.info "Cypher statements: $cypherStatements"
		def json = new JsonBuilder()
		
		json {statements cypherStatements}
		log.info json.toString()
		return json.toString()
	}

	/**
	 * Send a request to the database and return the results
	 * @param payload The JSON payload of the request
	 * @return the response JSON
	 */
	def sendDatabaseRequest (payload) {
		
		httpBuilder.request( POST, JSON ) {
		  uri.path = baseUri + '/transaction/commit'
		  requestContentType = 'application/json'
		  body = payload
		
		  response.success = { resp, json ->
			log.info "Query response: $json"
			return json
		  }
		}
	}
}
